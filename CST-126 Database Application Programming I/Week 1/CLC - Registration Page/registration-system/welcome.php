<link rel="stylesheet" href="css/style.css">
/*
  Project: login-system, Version: 1.0
  Module: welcome.php, Version: 1.0
  Programmer: Branden Manibusan, Date: 10/07/2018

  Main page with welcome screen!
*/
<?php session_start(); ?>
<div class="body content">
    <div class="welcome">
        <div class="alert alert-success"><?= $_SESSION['message'] ?></div>
        <img src="<?= $_SESSION['avatar'] ?>"><br />
        Welcome <span class="user"><?= $_SESSION['username'] ?></span>
        <?php
        $mysqli = new mysqli("localhost", "root", "#bmanibusan", "accounts_register");
        //Select queries return a resultset
        $sql = "SELECT username, avatar FROM users";
        $result = $mysqli->query($sql); //$result = mysqli_result object
        //var_dump($result);
        ?>
        <div id='registered'>
        <span>All registered users:</span>
        <?php
        while($row = $result->fetch_assoc()){ //returns associative array of fetched row
            //echo '<pre>';
            //print_r($row);
            //echo '</pre>';
            echo "<div class='userlist'><span>$row[username]</span><br />";
            echo "<img src='$row[avatar]'></div>";
        }
        ?>  
        </div>
    </div>
</div>
